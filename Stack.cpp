#include "Stack.h"
#include "ListStack.h"
#include "VectorStack.h"
#include "StackImplementation.h"

#include <stdexcept>

Stack::Stack(StackContainer container)
    : _containerType(container)
{
    switch (container)
    {
    case StackContainer::List: 
	{
        _pimpl = static_cast<IStackImplementation*>(new ListStack());    // конкретизируйте под ваши конструкторы, если надо
        break;
    }
    case StackContainer::Vector: 
	{
        _pimpl = static_cast<IStackImplementation*>(new VectorStack());    // конкретизируйте под ваши конструкторы, если надо
        break;
    }
    default:
	{
        throw std::runtime_error("Неизвестный тип контейнера");
	}
    }
}

Stack::Stack(const ValueType* valueArray, const size_t arraySize, StackContainer container)
{
    // принцип тот же, что и в прошлом конструкторе
	switch (container)
    {
    case StackContainer::List: 
	{
        _pimpl = static_cast<IStackImplementation*>(new ListStack(valueArray, arraySize));    // конкретизируйте под ваши конструкторы, если надо
        break;
    }
    case StackContainer::Vector: 
	{
        _pimpl = static_cast<IStackImplementation*>(new VectorStack(valueArray, arraySize));    // конкретизируйте под ваши конструкторы, если надо
        break;
    }
    default:
	{
        throw std::runtime_error("Неизвестный тип контейнера");
	}
    }
}

Stack::Stack(const Stack& copyStack)
{
	if (this != &copyStack)
	{
		*this = copyStack;	
	}
}

Stack& Stack::operator=(const Stack& copyStack)
{
	if( &copyStack == this ) { 
	    return *this;
    }
    this->_containerType = copyStack._containerType;
    delete _pimpl;
    switch ( this->_containerType ) {
		case StackContainer::List: {
    			  ListStack* tmpThis = new ListStack( *static_cast<ListStack*>(copyStack._pimpl) );
		          this->_pimpl = static_cast<IStackImplementation*>(tmpThis);
			  break;
		}	
		case StackContainer::Vector: {
			VectorStack* tmpThis = new VectorStack( *static_cast<VectorStack*>(copyStack._pimpl) );
		        this->_pimpl = static_cast<IStackImplementation*>(tmpThis);
			break;
		}
    	    	default:
		    throw std::runtime_error("Неизвестный тип контейнера");
    }
    return *this;

}

Stack::~Stack()
{
    delete _pimpl;        // композиция!
}

void Stack::push(const ValueType& value)
{
    // можно, т.к. push определен в интерфейсе
    _pimpl->push(value);
}

void Stack::pop()
{
    _pimpl->pop();
}

const ValueType& Stack::top() const
{
    return _pimpl->top();
}

bool Stack::isEmpty() const
{
    return _pimpl->isEmpty();
}

size_t Stack::size() const
{
    return _pimpl->size();
}
