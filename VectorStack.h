#pragma once
#include "StackImplementation.h"
#include <stdlib.h>

/// type of vector item
/// TODO: change to template
using ValueType = double;

class VectorStack : public IStackImplementation
{
public:
    // All c-tors and "=" operators make vectors 
    // where _capacity is equal to _size
    VectorStack() = default;
    VectorStack(const ValueType* rawArray, const size_t size, float coef = 2.0f);

    explicit VectorStack(const VectorStack& other);
    VectorStack& operator=(const VectorStack& other);

    explicit VectorStack(VectorStack&& other) noexcept;
    VectorStack& operator=(VectorStack&& other) noexcept;

	~VectorStack();

    void push(const ValueType& value);
    void pop();
	const ValueType& top() const;
	bool isEmpty() const;
    size_t size() const;

private:
    ValueType* _data = nullptr;
    size_t _size = 0;
    size_t _capacity = 0;
    float _multiplicativeCoef = 2.0f;
};
