#include "ListStack.h"

ListStack::Node::Node(ValueType val)
{
    value = val;
    next = nullptr;
}

size_t ListStack::size() const
{
    return _count;
}

bool ListStack::isEmpty() const
{
    return _count == 0;
}

const ValueType& ListStack::top() const
{
	if ( isEmpty() )
		throw -1;
	return _head->value;
}

void ListStack::pop()
{
   if ( !isEmpty() )
    {
        Node* next = _head->next;
        delete _head;
        _head = next;
        _count--;
    }
}

void ListStack::push(const ValueType& value)
{
    Node* node = new Node(value);
    if ( isEmpty() )
    {
        _head = node;
        _count++;
    }
    else
    {
        node->next = _head;
        _head = node;
        _count++;
    }
}

ListStack::ListStack(const ValueType* rawArray, const size_t size)
{
    while ( _count != size )
    {
        this->push(rawArray[_count]);
    }
}

ListStack::~ListStack()
{
    while ( _count != 0 )
    {
        Node* next = _head->next;
        delete _head;
        _head = next;
        _count--;
    }
}

ListStack::ListStack(const ListStack& other)
{
	if ( other.isEmpty() )
	{
		_head = nullptr;
		_count = 0;
		return;
	}
	Node* tmp = other._head;
	ValueType mass[other._count];
	for (int i = 0; i < other._count; ++i)
	{
		mass[i] = tmp->value;
		tmp = tmp->next;
	}
	for ( int i = other._count - 1; i >= 0; --i )
	{
		this->push(mass[i]);
	}
}

ListStack& ListStack::operator=(const ListStack& other)
{
	if ( other.isEmpty() )
	{
		_head = nullptr;
		_count = 0;
		return *this;
	}
	Node* tmp = other._head;
	ValueType mass[other._count];
	for ( int i = 0; i < other._count; ++i ) 
	{
		mass[i] = tmp->value;
		tmp = tmp->next;
	}
	for ( int i = other._count - 1; i >= 0; --i )
	{
		this->push(mass[i]);
	}
	return *this;
}

ListStack::ListStack(ListStack&& other) noexcept
{
	if ( other.isEmpty() )
	{
		_head = nullptr;
		_count = 0;
		return;
	}
	Node* tmp = other._head;
	while ( tmp != nullptr )
	{
		this->push(tmp->value);
		tmp = tmp->next;
		other.pop();
	}
}

ListStack& ListStack::operator=(ListStack&& other) noexcept
{
	if ( other.isEmpty() )
	{
		_head = nullptr;
		_count = 0;
		return *this;
	}
	Node* tmp = other._head;
	while ( tmp != nullptr )
	{
		this->push(tmp->value);
		tmp = tmp->next;
		other.pop();
	}
	return *this;
}
