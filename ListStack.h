#pragma once
#include <stdlib.h>
#include "StackImplementation.h"
using ValueType = double; 

class ListStack : public IStackImplementation
{


public:
	ListStack() = default;
	ListStack(const ValueType* rawArray, const size_t size);

    explicit ListStack(const ListStack& other);
    ListStack& operator=(const ListStack& other);

    explicit ListStack(ListStack&& other) noexcept;
    ListStack& operator=(ListStack&& other) noexcept;

	~ListStack();

    void push(const ValueType& value);
    void pop();
	const ValueType& top() const;
	bool isEmpty() const;
    size_t size() const;

private:
	struct Node
	{
		ValueType value;
		Node* next;
		Node(ValueType val);
	};

	Node* _head = nullptr;
	size_t _count = 0;
};
