#include "VectorStack.h"

VectorStack::VectorStack(const ValueType* rawArray, const size_t size, float coef)
{
	this->_data = new ValueType[size];
	for (size_t i = 0; i < size; ++i)
	{
		this->_data[i] = rawArray[i];
	}
	this->_size = size;
	this->_capacity = size;
	this->_multiplicativeCoef = coef;
}

VectorStack::~VectorStack()
{
	delete[] this->_data;
	this->_size = 0;
	this->_capacity = 0;
}

VectorStack::VectorStack(const VectorStack& other)
{
	if (this->_data != other._data){
		this->_data = new ValueType[other._size];
		this->_capacity = other._size;
		this->_size = other._size;
		for (size_t i = 0; i < this->_size; ++i)
		{
			this->_data[i] = other._data[i];
		}
	}
}

VectorStack& VectorStack::operator=(const VectorStack& other)
{
	if (this->_data != other._data){
		delete [] this->_data;
		this->_capacity = other._size;
		this->_size = other._size;
		this->_data = new ValueType[other._size];
		for (size_t i = 0; i < _size; ++i)
		{
			this->_data[i] = other._data[i];
		}
	}
	return *this;
}

VectorStack::VectorStack(VectorStack&& other) noexcept
{
	if (this->_data != other._data){
		this->_size = other._size;
		this->_capacity = other._size;
		this->_data = other._data;
		other._data = nullptr;
		other._capacity = 0;
		other._size = 0;
	}
}

VectorStack& VectorStack::operator=(VectorStack&& other) noexcept
{
	if (this->_data != other._data){
		delete [] this->_data;
		this->_size = other._size;
		this->_capacity = other._size;
		this->_data = other._data;
		other._data = nullptr;
		other._capacity = 0;
		other._size = 0;
	}
	return *this;
}

size_t VectorStack::size() const
{
	return this->_size;
}

void VectorStack::pop()
{
	
	if (this->_size == 0)
	{
		throw this->_size;
	}
	ValueType* newData = new ValueType[this->_capacity];
	this->_size--;
	for (size_t i = 0; i < this->_size; ++i)
	{
		newData[i] = this->_data[i];
		
	}
	this->_data = newData;
}

void VectorStack::push(const ValueType& value)
{
	if (this->_size == this->_capacity)
	{
		if (this->_capacity == 0)
		{
			this->_capacity = 1 * _multiplicativeCoef;
			ValueType* newData = new ValueType[this->_capacity];
			newData[this->_size] = value;
			this->_size += 1;
			this->_data = newData;
		}
		else
		{
			this->_capacity *= this->_multiplicativeCoef;
			ValueType* newData = new ValueType[this->_capacity];
			for (size_t i = 0; i < this->_size; ++i)
			{
				newData[i] = this->_data[i];
			}
			newData[_size] = value;
			this->_size += 1;
			this->_data = newData;
		}
	}
	else
	{
		this->_data[_size] = value;
		this->_size += 1;
	}
}

bool VectorStack::isEmpty() const
{
	if (this->_size == 0)
	{
		return true;
	}
	return false;
}

const ValueType& VectorStack::top() const
{
	if ( _size == 0)
		throw -1;
	return this->_data[_size - 1];
}
